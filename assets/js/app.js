
/*document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, options);
});*/

// Or with jQuery

//$('.dropdown-trigger').dropdown();
	

   $(document).ready(function(){
   		email_validation();
		cookie_time();
		var fixed_amount = $('input#fixed_amount').val();
    	if(fixed_amount != ''){
    		$('#card-collection-1 #credit_cart_amount_1').val(fixed_amount);
    		$('#card-collection-1 #credit_cart_amount_1').parents('.input-field').find('label').addClass('active');
    	}
    	
    });

	function email_validation(){   	
        var emailElem = $('#email')
        var emailValidMsgElem = $('#email-validity-message')


        $('#proceed-to-card').click(function(){
            if(validateEmail(emailElem)){
                localStorage.setItem("parentEmail", emailElem.val());
                window.location.href='card-collections.html'
                return;
            }
            emailElem.removeClass('valid').addClass('invalid')
            return;
        });
        emailElem.keyup(function(){
            if(validateEmail(emailElem)){
                emailElem.removeClass('invalid').addClass('valid')
                emailValidMsgElem.text($(this).data('success'))
                return;
            }
            emailElem.removeClass('valid').addClass('invalid')
            emailValidMsgElem.text($(this).data('error'))
            return;
        });

	            
    }

	function validateEmail(email){
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val()))
        {

            return true
        }

        return (false)
	}	

	function cookie_time(){

	   if(Cookies.get('cdTime') !== ''){
	   		var totalTime = Cookies.get('cdTime');
	   }else{
	   		var totalTime = $('#timer_set').val();
	   }	
	   
	   var halfTime = Math.floor(totalTime / 2);

	   if (!Cookies.get('cdTime')) {
	      var now = $.now(); // First time on page

			Cookies.set('CountExpired', 'no', {
				expires: 7,
				path: '/'
			});

	      Cookies.set('firstTime', now, {
	         expires: 7,
	         path: '/'
	      });
	      Cookies.set('cdTime', totalTime, {
	         expires: 7,
	         path: '/'
	      });
	      var runTimer = Cookies.get('cdTime');
	   } else {
	      var currentTime = $.now();
	      var usedTime = (currentTime - Cookies.get('firstTime')) / 1000; // Calculate and convert into seconds
	      var runTimer = Cookies.get('cdTime') - usedTime;
	   }
	   $('#cd').countdown({
	      until: runTimer,
	      compact: true,
	      onExpiry: EndCountdown,
	      onTick: Callbacks,
	      layout: 'Hai ancora {mn} minuti e {sn} secondi...'
	   });

	   function Callbacks(periods) {
	      if ($.countdown.periodsToSeconds(periods) === halfTime) {
	         $('#cd').addClass('halfway');
	      }
	      else if ($.countdown.periodsToSeconds(periods) <= 0) {
	         EndCountdown();
	      }
	   }

	   function EndCountdown() {
			Cookies.remove('CountExpired');
			Cookies.set('CountExpired', 'yes', {
				expires: 7,
				path: '/'
			});

			$('#cd').removeClass('halfway').addClass('ended');
			$('#cd').html('&#9829;');
	      	modal();

	   }
    }

	function modal(){
		$("#sticky").modal({
            escapeClose: false,
            clickClose: false,
            showClose: false
        });

	}	
    $(window).on('load',function(){
    	if(Cookies.get('CountExpired') == 'yes'){
    		modal();
    	}	
    	
    });

$(document).ready(function(){
        
        var numBtnCancel = 1;
        var numEmailBtnCancel = 1;
        var btnEqualFlag = 1;

        //select equal or custom option
        $('.btn-selected').click(function(){
            if($(this).attr('id') == 'btn-equal'){
                btnEqualFlag = 1
                $('#btn-equal').addClass('selected')
                $('#btn-custom').removeClass('selected')
                return false
            }
            if($(this).attr('id') == 'btn-custom'){
                btnEqualFlag = 2
                $('#btn-custom').addClass('selected')
                $('#btn-equal').removeClass('selected')
                return false
            }
        })

        //add card
        $("#btn-add-card").click(function(){
           
            numBtnCancel++ 

            $('.card-collection').last().clone().find('input').each(function(){
                $(this).attr({
                    'id': function(_, id) { 
                        var partialId = getPartialAttr(id, '_')
                        return partialId  + numBtnCancel
                    },
                    'name': function(_, name) {
                        var partialName = getPartialAttr(name, '_')
                        return partialName  + numBtnCancel
                     },
                    'value': function(_, value) {
                       if(btnEqualFlag == 1 && $(this).hasClass('credit_cart_amount') && $(this).val() != ''){
                           $(this).val(parseInt($(this).val())/numBtnCancel)
                           return
                       }
                        if(btnEqualFlag == 2 && $(this).hasClass('credit_cart_amount')  && $(this).val() != ''){
                            $(this).val(parseInt($(this).val()))
                            return
                        }

                        $(this).val('')
                        return
                    }          
                });
            }).end().appendTo('.card-form');
                $('.card-collection').last().attr('id', 'card-collection-' + numBtnCancel)
                $('.card-collection').last().attr('data-card', numBtnCancel)
                $('.card-delete-badge').last().attr('id','card-delete-badge-' + numBtnCancel).attr('data-del', numBtnCancel).removeClass('not-visible')
                $(".card-admin-badge").last().attr('id', 'card-admin-badge-' + numBtnCancel).addClass('not-visible')
                $(".credit_card_logo").last().attr('id', 'credit_card_logo_' + numBtnCancel).addClass('not-visible')
            })

        //remove card
            $('.card-form').on('click', '.delete-badge',function(){
                $('#card-collection-' + $(this).attr('data-del')).remove();
            });

        //add space to credit card number
        $('.card-form').on('keyup', '.credit_card_numbers', function(e){
            var val = $(this).val();
            var newval = '';
            val = val.replace(/\s/g, '');
            for(var i=0; i < val.length; i++) {
                if(i%4 == 0 && i > 0) newval = newval.concat(' ');
                newval = newval.concat(val[i]);
            }
            $(this).val(newval);

			$(this).validateCreditCard(function(result) {
				$(this).parent('.s6').find('#card_type').attr('class', '');
				$(this).parent('.s6').find('#card_valid').attr('class', '');
				if(result.card_type !== null && result.valid == true && result.length_valid == true && result.luhn_valid == true){  
				  $(this).parent('.s6').find('#card_type').attr('class', result.card_type.name);
				  $(this).parent('.s6').find('#card_valid').attr('class', 'valid');
				}			
			});

        })

        //format due date
        $('.card-form').on('keyup', '.credit_card_due', function(e){

            if(e.keyCode != 8){
                if($(this).val().length == 2){
                    $(this).val($(this).val() + '/')
                }

            }
            if($(this).val().length > 4){
                $(e.target).blur()
            }

        })
// add email
        $("#btn-add-email").click(function(){

            numEmailBtnCancel++

            $('.email-collection').last().clone().find('input').each(function(){
                $(this).attr({
                    'id': function(_, id) {
                        var partialId = getPartialAttr(id, '_')
                        return partialId  + numEmailBtnCancel
                    },
                    'name': function(_, name) {
                        var partialName = getPartialAttr(name, '_')
                        return partialName  + numEmailBtnCancel
                    },
                    'value': function(_, value) {

                        $(this).val('')
                        return
                    }
                });
            }).end().appendTo('.email-form');
            $('.email-collection').last().attr('id', 'email-collection-' + numEmailBtnCancel)
            $('.email-collection').last().attr('data-email',  numEmailBtnCancel)
            $('.email-delete-badge').last().attr('id','email-delete-badge-' + numEmailBtnCancel).attr('data-del', numEmailBtnCancel).removeClass('not-visible')
            $(".email-admin-badge").last().attr('id', 'email-admin-badge-' + numEmailBtnCancel).addClass('not-visible')

        })

        //delete email
        $('.email-form').on('click', '.email-delete-badge',function(){

            $('#email-collection-' + $(this).attr('data-del')).remove();
        });
        //store card

        $('#store-card').click(function(){
        	
            var cardInfoCollection = [];
            var emailInfoCollection = [];
            $('.card-collection').each(function(){

                var index = $(this).attr('data-card')

                var cardInfos = {
                    firstName: $('#first_name_' + index).val(),
                    lastName: $('#last_name_' + index).val(),
                    creditCardNumber: $('#credit_card_numbers_' + index).val(),
                    dueDate: $('#credit_card_due_date_' + index).val(),
                    cvv: $('#credit_card_cvv_' + index).val(),
                    amount: $('#credit_cart_amount_' + index).val(),
                }

                cardInfoCollection.push(cardInfos);
            })
            $('.email-collection').each(function(){

                var index = $(this).attr('data-email')

                var emailInfos = {
                    invitationEmail: $('#invitation_email_' + index).val(),
                    invitationMobilePhone: $('#invitation_mobile_phone_' + index).val(),
                    invitationAmount: $('#invitation_amount_' + index).val(),
                }

                emailInfoCollection.push(emailInfos);
            })

            var cardCollectionByUser = {
                userName: localStorage.getItem("parentEmail"),
                cardInfo: cardInfoCollection,
                emailInfo: emailInfoCollection
            }


            Cookies.set('cardInfoCollection', cardInfoCollection, {
				expires: 7,
				path: '/'
			});

            Cookies.set('emailInfoCollection', emailInfoCollection, {
				expires: 7,
				path: '/'
			});

            Cookies.set('cardCollectionByUser', cardCollectionByUser, {
				expires: 7,
				path: '/'
			});			
			if(validate_field()){            
            	localStorage.setItem('cardAndEmailInfo',cardCollectionByUser);
            	window.location.href='review.html';
            }
        });

        var inviteElem = $('#email')
        var emailValidMsgElem = $('#email-validity-message')


    });


    function getPartialAttr(fullAttr, seperator){
        var lastOccurance = fullAttr.lastIndexOf(seperator) + 1
        var fullAttr = fullAttr.substr(0,lastOccurance)

        return fullAttr;
    }

    function validate_field(){
    	var valid = true;
        
		$('form input').each(function() {
		        var $this = $(this);	        
	        if(!$this.val()) {
	            $this.removeClass('invalid');
	            var inputName = $this.attr('name');
	            valid = false;
	            $this.addClass('invalid');
	        }
		});

		return valid;
    }

    function read_form_cookie(){
    	var cardCollectionByUser = Cookies.get('cardCollectionByUser');
    	var cardInfoCollection = Cookies.get('cardInfoCollection');
    	var emailInfoCollection = Cookies.get('emailInfoCollection');
    	var user_credit_area_insert = '';
		$.each(JSON.parse(cardInfoCollection), function(index, value){
			console.log(cardInfoCollection);
			var firstName = value.firstName; 
			var lastName = value.lastName; 
			var creditCardNumber = value.creditCardNumber;
			var dueDate = value.dueDate;
			var cvv = value.cvv;
			var amount = value.amount;

			user_credit_area_insert += '<div class="row main-header">'+
			'<form class="col s12"><div class="header-credit pre-authorized-succes" >'+
			 '<div class="sbu-header ">Dati carta di credito<span class="admin-badge"><i class="material-icons">grade</i></span></div>'+
			'<div class="row"><div class="input-field col s6"><input id="first_name disabled" disabled="" value="'+ firstName +'" type="text" class="validate"><label for="first_name disabled" class="active">Il tuo Nome</label></div><div class="input-field col s6"><input id="last_name disabled" disabled="" value="'+ lastName +'" type="text" class="validate"> <label for="last_name disabled" class="active">Il tuo Cognome</label></div> </div> <div class="row"><div class="input-field col s6"><input id="credit_card_numbers disabled" disabled="" value="'+creditCardNumber+'" type="text" class="validate credit_numbers"> <label for="credit_card_numbers disabled" class="active">Numero carta di credito</label>     </div>     <div class="input-field col s2"> <input id="credit_card_due_date disabled" disabled="" value="'+dueDate+'" type="text" class="validate  credit_numbers"><label for="credit_card_due_date disabled" class="active">Scadenza</label> </div> <div class="input-field col s2"> <input id="credit_card_cvv disabled" disabled="" value="'+cvv+'" type="text" class="validate credit_numbers"> <label for="credit_card_cvv disabled" class="active">CVV</label></div>     <div class="input-field col s2"> <input id="credit_cart_amount disabled" disabled="" value="€ '+amount+'" type="text"  class="validate credit_numbers"><label for="credit_cart_amount disabled" class="active">Importo</label> </div> </div></div></form></div>';
		});
		 $(".user_credit_area_insert").html(user_credit_area_insert);


		var email_collection_html = '';
		$.each(JSON.parse(emailInfoCollection), function(index, value){
			
			var email = value.invitationEmail; 
			var amount = value.invitationAmount; 
			var phone = value.invitationMobilePhone;

			email_collection_html += '<div class="input-field col s4"><input id="invitation_email disabled" disabled="" value="'+email+'" type="email" class="validate"><label for="invitation_email disabled" class="active">Indirizzo email</label></div><div class="input-field col s4"><input id="invitation_mobile_phone disabled" disabled="" value="'+phone+'" type="tel" class="validate"><label for="invitation_mobile_phone disabled" class="active">Cellulare</label></div><div class="input-field col s4"><input id="invitation_amount disabled" disabled="" value="€ '+amount+'" type="text" class="validate"><label for="invitation_amount disabled" class="active">Importo</label></div>';
			//invitation_area_insert
            //$("#result").append(index + ": " + value + '<br>');
        });

       $(".invitation_area_insert").html(email_collection_html);

    }

    read_form_cookie();

    function remove_cookie(){
		Cookies.remove('cardInfoCollection');
		Cookies.remove('emailInfoCollection');
		Cookies.remove('cardCollectionByUser');
		Cookies.remove('cdTime');
		Cookies.remove('firstTime');
    }

    function redirect_orginal_path(){

    }